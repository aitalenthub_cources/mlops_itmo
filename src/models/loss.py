import torch
import torch.nn as nn


class ContrastiveCrossEntropy(nn.Module):
    def __init__(self, margin=0.5):
        super().__init__()
        self.margin = margin

    def forward(self, vac_emb: torch.Tensor, pos_emb: torch.Tensor, neg_emb: torch.Tensor) -> torch.Tensor:
        pos_scores = (vac_emb * pos_emb).sum(dim=1)
        neg_scores = (vac_emb * neg_emb).sum(dim=1)

        loss_val = torch.exp(neg_scores + self.margin) - pos_scores
        loss_val[loss_val < 1] = 1

        return loss_val.log().mean()
