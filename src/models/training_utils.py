import torch.functional as F

from operator import itemgetter
from sentence_transformers import SentenceTransformer


def get_base_model(model_path):
    model = SentenceTransformer(model_path)

    return model


def collate_fn(batch):
    vacancies = list(map(itemgetter(0), batch))
    positives = list(map(itemgetter(1), batch))
    negatives = list(map(itemgetter(2), batch))

    return vacancies, positives, negatives


def get_embeddings(model, input, normalize=False):
    out = model(input)['sentence_embedding']

    if normalize:
        out = F.normalize(out, dim=-1)

    return out


def to_device(tokens, device):
    return {k: v.to(device) for k, v in tokens.items()}
