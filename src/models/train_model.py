import argparse

import mlflow
from tqdm import tqdm
from torch.optim import AdamW
from torch.utils.data import DataLoader

from src.data.matching_dataset import TripletDataset
from src.models.loss import ContrastiveCrossEntropy
from src.models.training_utils import (collate_fn,
                                       get_base_model,
                                       get_embeddings,
                                       to_device)


def setup_mlflow(mlflow_url: str, mlflow_experiment_name: str):
    mlflow.set_tracking_uri(mlflow_url)
    mlflow.set_experiment(mlflow_experiment_name)
    mlflow.autolog()


def parse_args():
    parser = argparse.ArgumentParser(description="Train a cv-vacancies encoder")
    parser.add_argument(
        "--model_path",
        default="distiluse-base-multilingual-cased-v1",
        help="huggingface path to model"
    )
    parser.add_argument(
        "--save_name",
        default="ce-model",
        help="path to save model"
    )
    parser.add_argument(
        "--tracking_uri",
        default="http://92.246.76.194:5000",
        help="path to save model"
    )
    parser.add_argument(
        "--exp_name",
        default="matching model training",
        help="path to save model"
    )
    parser.add_argument(
        "--dataset_path",
        default="data/processed/train.csv",
        help="huggingface path to model"
    )
    parser.add_argument(
        "--batch-size",
        default=1,
        help="train batch size"
    )
    parser.add_argument(
        "--margin",
        default=0.75,
        help="margin"
    )
    parser.add_argument(
        "--epochs",
        default=1,
        help="num epochs to train"
    )
    parser.add_argument(
        "--lr",
        default=1e-4,
        help="learning rate"
    )
    parser.add_argument(
        "--device",
        default="mps",
        help="device"
    )
    parser.add_argument(
        "--verbose",
        default=2,
        help="iterations between logs"
    )

    args = parser.parse_args()

    return args


def train_epoch(model, dataloader, loss, optimizer, device, epoch):
    losses = []

    pbar = tqdm(enumerate(dataloader), total=len(dataloader))

    for i, (vacancies, positives, negatives) in pbar:
        vac_emb = get_embeddings(model, to_device(model.tokenize(vacancies), device))
        pos_emb = get_embeddings(model, to_device(model.tokenize(positives), device))
        neg_emb = get_embeddings(model, to_device(model.tokenize(negatives), device))

        loss_value = loss(vac_emb, pos_emb, neg_emb)
        loss_value.backward()
        losses.append(loss_value.item())

        pbar.set_postfix_str(f"Loss: {loss_value.item():.4f}")

        mlflow.log_metric("batch_loss", loss_value.item(), step=i)

        optimizer.step()
        optimizer.zero_grad()

        if i == 100:
            break

    print(f"epoch loss: {(sum(losses) / len(losses)):4f}")
    mlflow.log_metric("epoch_loss", sum(losses) / len(losses), step=epoch)

    return model, loss, optimizer


def main():
    args = parse_args()

    print("Loading model...")
    model = get_base_model(args.model_path)
    model.to(args.device)
    model.train()
    print("Setup training...")

    setup_mlflow(args.tracking_uri, args.exp_name)
    with mlflow.start_run(run_name='experiment'):
        mlflow.log_params({
            'model': args.model_path,
            'lr': args.lr,
            'margin': args.margin,
            'epochs': args.epochs
        })

        loss = ContrastiveCrossEntropy(margin=args.margin)
        optimizer = AdamW(model.parameters(), lr=args.lr)

        dataloader = DataLoader(TripletDataset(args.dataset_path),
                                batch_size=args.batch_size,
                                shuffle=True,
                                collate_fn=collate_fn)
        print("Train model...")
        pbar = tqdm(range(args.epochs))
        for epoch in pbar:
            pbar.set_postfix_str(f"Epoch: {epoch}")
            model, loss, optimizer = train_epoch(model, dataloader, loss, optimizer, args.device, epoch)

        print("Saving model...")
        mlflow.sentence_transformers.log_model(
            model=model,
            artifact_path=args.save_name,
        )


if __name__ == '__main__':
    main()
