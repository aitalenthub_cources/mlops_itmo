import click
import pandas as pd


@click.command()
@click.argument("input_dataset_file", type=click.Path(readable=True))
@click.argument("output_dataset_file", type=click.Path(writable=True))
def cli(input_dataset_file: str, output_dataset_file: str) -> None:
    df_vacancies = pd.read_csv(input_dataset_file)

    df_vacancies['min_years'] = df_vacancies['Experience'].replace({
        'От 3 до 6 лет': 3,
        'От 1 года до 3 лет': 1,
        'Нет опыта': 0,
        'Более 6 лет': 6
    })
    df_vacancies['Name'] = df_vacancies['Name'].str.lower()

    df_vacancies['Name'] = df_vacancies['Name'].str.split('/').apply(lambda x: x[0])

    df_vacancies.to_csv(output_dataset_file, index=False)


if __name__ == "__main__":
    cli()
