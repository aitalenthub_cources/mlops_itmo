import pandas as pd

from torch.utils.data import Dataset


class TripletDataset(Dataset):
    def __init__(self, dataset_path: str) -> None:
        super().__init__()
        self.dataset_path = dataset_path

        self.data = self._prepare_set()

    def __len__(self):
        return len(self.data)

    def _prepare_set(self):
        df = pd.read_csv(self.dataset_path)

        df['Keys'] = df['Keys'].apply(eval)
        df['skills'] = df['Keys'].apply(lambda x: 'Требуемые навыки: ' + ', '.join(x))
        df['Specializations'] = df['Specializations'].apply(eval)
        df['Specializations'] = df['Specializations'].apply(lambda x: ', '.join(x))
        df['job_description'] = df['Description'] + ' ' + df['skills'] + ' ' + df[
            'Specializations'] + '. Требуемый опыт:' + df['Experience']

        df['resume_description'] = 'Ищет работу на должность: ' + df['Ищет работу на должность:'] + ' ' + df[
            'Опыт работы'] + ' ' + df['Образование и ВУЗ']

        return df

    def __getitem__(self, index):
        vacancy = self.data.iloc[index]['job_description']
        positive_cv = self.data.iloc[index]['resume_description']
        negative_cv = self._sample_negative(index)

        return vacancy, positive_cv, negative_cv

    def _sample_negative(self, index):
        vacancy_name = self.data.iloc[index]['Name']
        negative_row = self.data.sample()

        while negative_row["Name"].item() == vacancy_name:
            negative_row = self.data.sample()

        return negative_row['resume_description'].item()
