import click
import pandas as pd


def get_experience(exp_str: str) -> str:
    exp_lst = exp_str.split()
    if len(exp_lst) <= 2:
        return 0
    if 'лет' not in ' '.join(exp_lst[:5]) and 'год' not in ' '.join(exp_lst[:5]):
        return int(exp_lst[2]) / 12
    if 'месяц' in ' '.join(exp_lst[:5]):
        return int(exp_lst[2]) + int(exp_lst[4]) / 12
    else:
        return int(exp_lst[2])


def get_education(edu_list: list) -> str:
    final = list()
    for word in edu_list:
        if not word.isalpha():
            break

        final.append(word)

    return ' '.join(final)


@click.command()
@click.argument("input_dataset_file", type=click.Path(readable=True))
@click.argument("output_dataset_file", type=click.Path(writable=True))
def cli(input_dataset_file: str, output_dataset_file: str) -> None:
    df_resume = pd.read_csv(input_dataset_file, delimiter=';')

    df_resume['Ищет работу на должность:'] = df_resume['Ищет работу на должность:'].str.split(', ')
    df_resume = df_resume.explode('Ищет работу на должность:')
    df_resume['Ищет работу на должность:'] = df_resume['Ищет работу на должность:'].str.lower()

    df_resume = df_resume.reset_index().dropna()
    df_resume['years'] = df_resume['Опыт работы'].apply(lambda x: get_experience(x))
    df_resume['city'] = df_resume['Город, переезд, командировки'].str.split(',').apply(lambda x: x[0])
    df_resume['education'] = df_resume['Образование и ВУЗ'].str.split().apply(get_education)

    df_resume.to_csv(output_dataset_file, index=False)


if __name__ == "__main__":
    cli()
