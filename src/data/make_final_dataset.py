# -*- coding: utf-8 -*-
import click
import pandas as pd

from sklearn.model_selection import train_test_split


@click.command()
@click.argument("input_vacancies_file", type=click.Path(readable=True))
@click.argument("input_resumes_file", type=click.Path(readable=True))
@click.argument("output_train_file", type=click.Path(writable=True))
@click.argument("output_test_file", type=click.Path(writable=True))
def cli(input_vacancies_file: str, input_resumes_file: str, output_train_file: str, output_test_file: str):
    df_vacancies = pd.read_csv(input_vacancies_file)
    df_resumes = pd.read_csv(input_resumes_file)

    job_resume = df_vacancies.merge(df_resumes, left_on='Name', right_on='Ищет работу на должность:', how='left')
    job_resume = job_resume.dropna(subset=['Ищет работу на должность:'])
    job_resume = job_resume.drop_duplicates(subset=['Ids', 'index'])

    counts = job_resume['Ids'].value_counts()
    counts = counts[counts >= 9]  # conflict 1

    job_resume = job_resume[job_resume['Ids'].isin(counts.index)]

    job_resume = job_resume[job_resume['years'] >= job_resume['min_years']]
    job_resume = job_resume[(job_resume['city'] == job_resume['Area']) | (job_resume['Schedule'] == 'Удаленная работа')]

    train, val = train_test_split(job_resume, test_size=0.1, random_state=42)

    train.to_csv(output_train_file, index=False)
    val.to_csv(output_test_file, index=False)


if __name__ == '__main__':
    cli()
